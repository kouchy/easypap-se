#include "kernel/ocl/common.cl"

__kernel void pixelize_ocl (__global unsigned *in)
{
  int x = get_global_id (0);
  int y = get_global_id (1);

  unsigned color = in [y * DIM + x];

  // TODO

  in [y * DIM + x] = color;
}
