#include "easypap.h"

static float *tgrid0;
static float *tgrid1;

// ============================================================================
// ======================================================================== SEQ
// ============================================================================

EXTERN void heat_init() {
  tgrid0 = (float *)malloc(sizeof(float) * DIM * DIM);
  tgrid1 = (float *)malloc(sizeof(float) * DIM * DIM);

  for (unsigned i = 0; i < DIM * DIM; i++) {
    tgrid0[i] = 0.f;
    tgrid1[i] = 0.f;
  }

  unsigned ss = DIM / 2;  // square size
  unsigned shift = DIM / 16;

  for (unsigned i = 0; i < ss; i++)
    for (unsigned j = 0; j < ss; j++)
      tgrid0[(j + shift) * DIM + i + shift] = 1.f;
}

///////////////////////////// Sequential version (tiled)
// Suggested cmdline(s):
// ./run -k heat -v seq -si
//
EXTERN int heat_do_tile_default(int x, int y, int width, int height) {
  float c,l,r,b,t;
  for (int i = y; i < y + height; i++)
    for (int j = x; j < x + width; j++) {
      c =                      tgrid0[(i + 0) * DIM + (j + 0)];
      l = (j - 1) >=       0 ? tgrid0[(i + 0) * DIM + (j - 1)] : tgrid0[  (i + 0) * DIM + (DIM - 1)];
      r = (j + 1) < (int)DIM ? tgrid0[(i + 0) * DIM + (j + 1)] : tgrid0[(  i + 0) * DIM + (      0)];
      b = (i - 1) >=       0 ? tgrid0[(i - 1) * DIM + (j + 0)] : tgrid0[(DIM - 1) * DIM + (  j + 0)];
      t = (i + 1) < (int)DIM ? tgrid0[(i + 1) * DIM + (j + 0)] : tgrid0[(      0) * DIM + (  j + 0)];
      tgrid1[i * DIM + j] = (c + l + r + t + b) * 0.2f;
    }

  return 0;
}

///////////////////////////// Sequential version (tiled)
// Suggested cmdline(s):
// ./run -k heat -v seq -r 1000
// ./run -k heat -v seq -i 10000 --no-display
//
EXTERN unsigned heat_compute_seq(unsigned nb_iter) {
  for (unsigned it = 1; it <= nb_iter; it++) {
    do_tile(0, 0, DIM, DIM, 0);

    float *tmp = tgrid1;
    tgrid1 = tgrid0;
    tgrid0 = tmp;
  }

  for (unsigned i = 0; i < DIM; i++)
    for (unsigned j = 0; j < DIM; j++)
      cur_img(i, j) = heat_to_rgb(tgrid0[i * DIM + j]);

  return 0;
}

// ============================================================================
// ================================================================== BORDER V2
// ============================================================================

// EXTERN void heat_init_seq_bv2() {
//   tgrid0 = (float *)malloc(sizeof(float) * /* TODO */);
//   tgrid1 = (float *)malloc(sizeof(float) * /* TODO */);

//   for (unsigned i = 0; i < /* TODO */; i++) {
//     tgrid0[i] = 0.f;
//     tgrid1[i] = 0.f;
//   }

//   unsigned ss = DIM / 2;  // square size
//   unsigned shift = DIM / 16;

//   for (unsigned i = 0; i < ss; i++)
//     for (unsigned j = 0; j < ss; j++)
//       tgrid0[((/* TODO */) + shift) * (/* TODO */) + (/* TODO */) + shift] = 1.f;
// }

// EXTERN int heat_do_tile_bv2(int x, int y, int width, int height) {
//   float c,l,r,b,t;
//   for (int i = y; i < y + height; i++)
//     for (int j = x; j < x + width; j++) {
//       c = tgrid0[(/* TODO */ + 0) * (/* TODO */) + (/* TODO */ + 0)];
//       l = tgrid0[(/* TODO */ + 0) * (/* TODO */) + (/* TODO */ - 1)];
//       r = tgrid0[(/* TODO */ + 0) * (/* TODO */) + (/* TODO */ + 1)];
//       b = tgrid0[(/* TODO */ - 1) * (/* TODO */) + (/* TODO */ + 0)];
//       t = tgrid0[(/* TODO */ + 1) * (/* TODO */) + (/* TODO */ + 0)];
//       tgrid1[(/* TODO */) * (/* TODO */) + (/* TODO */)] = (c + l + r + t + b) * 0.2f;
//     }

//   return 0;
// }

// // ./run -k heat -v seq_bv2 -wt bv2 -r 1000
// // ./run -k heat -v seq_bv2 -wt bv2 -i 10000 --no-display
// EXTERN unsigned heat_compute_seq_bv2(unsigned nb_iter) {
//   for (unsigned it = 1; it <= nb_iter; it++) {
//     // copie des bords
//     for (int i = 0; i < (int)DIM; i++) {
//       tgrid0[       0  * (DIM + 2) +   i + 1] = tgrid0[   DIM  * (DIM + 2) + i + 1]; // top   <= bot
//       tgrid0[(DIM + 1) * (DIM + 2) +   i + 1] = tgrid0[     1  * (DIM + 2) + i + 1]; // bot   <= top
//       tgrid0[  (i + 1) * (DIM + 2)       + 0] = tgrid0[(i + 1) * (DIM + 2) +   DIM]; // left  <= right
//       tgrid0[  (i + 1) * (DIM + 2) + DIM + 1] = tgrid0[(i + 1) * (DIM + 2) +     1]; // right <= left
//     }

//     do_tile(0, 0, DIM, DIM, 0);

//     float *tmp = tgrid1;
//     tgrid1 = tgrid0;
//     tgrid0 = tmp;
//   }

//   for (unsigned i = 0; i < DIM; i++)
//     for (unsigned j = 0; j < DIM; j++)
//       cur_img(i, j) = heat_to_rgb(tgrid0[(/* TODO */) * (/* TODO */) + (/* TODO */)]);

//   return 0;
// }

// ============================================================================
// ===================================================================== MPI v0
// ============================================================================
#ifdef ENABLE_MPI

#include <mpi.h>

static int mpi_y = -1;
static int mpi_h = -1;
// static int mpi_rank = -1;
// static int mpi_size = -1;

// EXTERN void heat_init_mpi_v0(void) {
//   heat_init_seq_bv2();

//   easypap_check_mpi();  // check if MPI was correctly configured

//   MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
//   MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

//   mpi_y = mpi_rank * (DIM / /* TODO */);
//   mpi_h = (DIM / /* TODO */);

//   PRINT_DEBUG('M', "In charge of slice [%d-%d]\n", mpi_y,
//               mpi_y + mpi_h - 1);
// }

// // ./run -k heat -v mpi_v0 --mpirun "-np 2" -wt bv2 -r 1000 --debug-flags M
// // ./run -k heat -v mpi_v0 --mpirun "-np 8" -wt bv2 -i 10000 --no-display
// EXTERN unsigned heat_compute_mpi_v0(unsigned nb_iter) {
//   for (unsigned it = 1; it <= nb_iter; it++) {
//     // copie des bords verticaux
//     for (int i = mpi_y; i < mpi_y + mpi_h; i++) {
//       tgrid0[(i + 1) * (DIM + 2)       + 0] = tgrid0[(i + 1) * (DIM + 2) + DIM]; // left  <= right
//       tgrid0[(i + 1) * (DIM + 2) + DIM + 1] = tgrid0[(i + 1) * (DIM + 2) +   1]; // right <= left
//     }

//     do_tile(0, mpi_y, DIM, mpi_h, 0);

//     float *tmp = tgrid1;
//     tgrid1 = tgrid0;
//     tgrid0 = tmp;

//     /* TODO: écrire les communications MPI ici */
//   }

//   for (int i = /* TODO */; i < /* TODO */; i++)
//     for (int j = 0; j < (int)DIM; j++)
//       cur_img(i, j) = heat_to_rgb(tgrid0[(i + 1) * (DIM + 2) + (j + 1)]);

//   return 0;
// }

// ============================================================================
// ===================================================================== MPI v1
// ============================================================================

EXTERN void heat_init_mpi_v1(void) {
  // heat_init_seq_bv2();
  // heat_init_mpi_v0();
}

EXTERN unsigned heat_compute_mpi_v1(unsigned nb_iter) {
  for (unsigned it = 1; it <= nb_iter; it++) {
    // copie des bords verticaux
    for (int i = mpi_y; i < mpi_y + mpi_h; i++) {
      tgrid0[(i + 1) * (DIM + 2)       + 0] = tgrid0[(i + 1) * (DIM + 2) + DIM]; // left  <= right
      tgrid0[(i + 1) * (DIM + 2) + DIM + 1] = tgrid0[(i + 1) * (DIM + 2) +   1]; // right <= left
    }

    do_tile(0, mpi_y, DIM, mpi_h, 0);

    float *tmp = tgrid1;
    tgrid1 = tgrid0;
    tgrid0 = tmp;

    /* TODO */
  }

  /* TODO */

  return 0;
}

// ============================================================================
// ===================================================================== MPI v2
// ============================================================================

EXTERN void heat_init_mpi_v2(void) {
  // heat_init_seq_bv2();
  // heat_init_mpi_v0();
}

EXTERN unsigned heat_compute_mpi_v2(unsigned nb_iter) {
  for (unsigned it = 1; it <= nb_iter; it++) {
    // copie des bords verticaux
    for (int i = mpi_y; i < mpi_y + mpi_h; i++) {
      tgrid0[(i + 1) * (DIM + 2)       + 0] = tgrid0[(i + 1) * (DIM + 2) + DIM]; // left  <= right
      tgrid0[(i + 1) * (DIM + 2) + DIM + 1] = tgrid0[(i + 1) * (DIM + 2) +   1]; // right <= left
    }

    do_tile(0, mpi_y, DIM, mpi_h, 0);

    float *tmp = tgrid1;
    tgrid1 = tgrid0;
    tgrid0 = tmp;

    /* TODO */
  }

  /* TODO */

  return 0;
}

#endif